import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:ml_experiments/common/TouchableNativeFeedBack.dart';

class HomePage extends StatelessWidget {
  final Widget child;

  HomePage({Key key, this.child}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: TouchableNativeFeedBack(
        onTap: () => Navigator.pushNamed(context, '/vision'),
        color: Colors.redAccent,
        child: Text(
          "Vision",
          style: TextStyle(
              color: Colors.white, fontSize: 24, fontWeight: FontWeight.w500),
        ),
      ),
    );
  }
}
