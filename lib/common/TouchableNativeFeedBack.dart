import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class TouchableNativeFeedBack extends StatelessWidget {
  const TouchableNativeFeedBack(
      {Key key,
      this.child,
      this.onTap,
      this.color,
      this.borderRadius,
      this.decoration,
      this.shape})
      : super(key: key);

  final Widget child;
  final Function onTap;
  final Color color;
  final Decoration decoration;
  final RoundedRectangleBorder shape;
  final BorderRadius borderRadius;

  @override
  Widget build(BuildContext context) {
    if (Theme.of(context).platform == TargetPlatform.iOS) {
      return CupertinoButton(
        padding: EdgeInsets.all(0),
        borderRadius: this.borderRadius,
        onPressed: this.onTap,
        color: this.color,
        child: Container(
          child: this.child,
        ),
        pressedOpacity: 0.2,
      );
    } else if (Theme.of(context).platform == TargetPlatform.android) {
      return ButtonTheme(
        minWidth: 0.0,
        child: RaisedButton(
            padding: EdgeInsets.all(0),
            child: this.child,
            onPressed: this.onTap,
            color: this.color != null ? this.color : Colors.transparent,
            elevation: 0,
            highlightElevation: 0,
            shape: this.shape != null ? this.shape : null),
      );
    } else
      return null;
  }
}
